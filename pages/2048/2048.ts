import { gsap } from 'gsap';
import { Slide, Direction, Position } from './model';
import Vue from 'vue';

export default Vue.extend({
  name: 'game2048',

  components: {
    Keypress: () => import('vue-keypress')
  },

  head() {
    return {
      link: [{
        hid: 'icon',
        rel: 'icon',
        type: 'image/x-icon',
        href: '2048-favicon.ico'
      }]
    }
  },

  data () {
    let slides: Slide[] = [];
    return {
      slides,
      gameover: false,
      score: 0,
      easyMode: false,
      currentSlide: 0,
      mergedLastMove: false,
      arrowKeys: [
        {keyCode: Direction.Left, modifiers: [], preventDefault: true},
        {keyCode: Direction.Up, modifiers: [], preventDefault: true},
        {keyCode: Direction.Right, modifiers: [], preventDefault: true},
        {keyCode: Direction.Down, modifiers: [], preventDefault: true}
      ]
    }
  },

  mounted () {
  },

  methods: {
    startNewGame(): void {},

    handleArrowKey(keydown: any): void {
      switch(keydown.event.keyCode) {
        case Direction.Left:
          break;
        case Direction.Right:
          break;
        case Direction.Up:
          break;
        case Direction.Down:
          break;
      }
    },
  }
})
