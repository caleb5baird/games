
export enum Direction {
  Left = 37,
  Up,
  Right,
  Down
}

export interface Position {
  x: number;
  y: number;
}

export interface Slide {
  value: number;
  id: number;
  merged?: number;
  position: Position;
}
