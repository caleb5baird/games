import { gsap } from 'gsap';
import { Section, Direction, Position } from './model';
import Vue from 'vue';

export default Vue.extend({
  name: 'Snake',

  components: {
    Keypress: () => import('vue-keypress')
  },

  data () {
    let snake: Section[] = [];
    return {
      snake,
      height: window.innerHeight,
      width: window.innerWidth,
      sectionSize: 10,
      snakeSpeed: 1,
      stop: false,
      score: 0,
      direction: Direction.Up,
      sectionId: 0,
      arrowKeys: [
        {keyCode: Direction.Left, modifiers: [], preventDefault: true},
        {keyCode: Direction.Up, modifiers: [], preventDefault: true},
        {keyCode: Direction.Right, modifiers: [], preventDefault: true},
        {keyCode: Direction.Down, modifiers: [], preventDefault: true}
      ]
    }
  },

  mounted () {
    this.$nextTick(() => {
      window.addEventListener('resize', this.onResize);
    });

    this.startNewGame();
    window.setInterval(this.moveSnake, 1000*(1/this.snakeSpeed));
  },

  beforeDestroy() { window.removeEventListener('resize', this.onResize) },

  computed: {
    cssVars(): any {
      return {
        '--section-size': this.sectionSize + 'px',
        '--snake-speed': 1/this.snakeSpeed + 's',
      }
    }
  },

  methods: {
    getSectionStyle(section: Section) {
      return {
        ...this.cssVars,
        top: section.position.y+'px',
        left: section.position.x+'px',
      }
    },

    // Update height and width width the window
    onResize() {
      this.height = window.innerHeight;
      this.width = window.innerWidth;
    },


    startNewGame(): void {
    },

    moveSnake() {
    },

    handleArrowKey(keydown: any): void {
      switch(keydown.event.keyCode) {
        case Direction.Left:
          break;
        case Direction.Right:
          break;
        case Direction.Up:
          break;
        case Direction.Down:
          break;
        default:
          // Unrecognized key do nothing
      }
    },
  }
})
