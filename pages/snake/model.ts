
export enum Direction {
  Left = 37,
  Up,
  Right,
  Down
}

export interface Position {
  x: number;
  y: number;
}

export interface Section {
  direction: Direction,
  position: Position;
  id: number;
  corner?: boolean;
  head?: boolean;
}
