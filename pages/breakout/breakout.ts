import { gsap } from 'gsap';
import { Direction, Position } from './model';
import Vue from 'vue';

export default Vue.extend({
  name: 'breakout',

  components: {
    Keypress: () => import('vue-keypress')
  },

  data () {
    return {
      gameover: false,
      score: 0,
      arrowKeys: [
        {keyCode: Direction.Left, modifiers: [], preventDefault: true},
        {keyCode: Direction.Up, modifiers: [], preventDefault: true},
        {keyCode: Direction.Right, modifiers: [], preventDefault: true},
        {keyCode: Direction.Down, modifiers: [], preventDefault: true}
      ]
    }
  },

  mounted () {
  },

  methods: {

    startNewGame(): void {
    },

    handleArrowKey(keydown: any): void {
      switch(keydown.event.keyCode) {
        case Direction.Left:
          break;
        case Direction.Right:
          break;
        case Direction.Up:
          break;
        case Direction.Down:
          break;
      }
    },
  }
})
